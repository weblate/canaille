# Translations template for PROJECT.
# Copyright (C) 2023 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2023.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2023-01-22 13:42+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.11.0\n"

#: canaille/account.py:103 canaille/account.py:128
#: canaille/oidc/endpoints.py:81
msgid "Login failed, please check your information"
msgstr ""

#: canaille/account.py:134
#, python-format
msgid "Connection successful. Welcome %(user)s"
msgstr ""

#: canaille/account.py:147
#, python-format
msgid "You have been disconnected. See you next time %(user)s"
msgstr ""

#: canaille/account.py:165
msgid "Could not send the password initialization link."
msgstr ""

#: canaille/account.py:170
msgid ""
"A password initialization link has been sent at your email address. You "
"should receive it within a few minutes."
msgstr ""

#: canaille/account.py:176 canaille/account.py:426
msgid "Could not send the password initialization email"
msgstr ""

#: canaille/account.py:265 canaille/account.py:348
msgid "User account creation failed."
msgstr ""

#: canaille/account.py:286 canaille/account.py:314
msgid "The invitation link that brought you here was invalid."
msgstr ""

#: canaille/account.py:293
msgid "The invitation link that brought you here has expired."
msgstr ""

#: canaille/account.py:300
msgid "Your account has already been created."
msgstr ""

#: canaille/account.py:307
msgid "You are already logged in, you cannot create an account."
msgstr ""

#: canaille/account.py:326 canaille/forms.py:211 canaille/forms.py:273
#: canaille/templates/groups.html:11 canaille/templates/userlist.html:17
#: canaille/themes/default/base.html:61
msgid "Groups"
msgstr ""

#: canaille/account.py:353
msgid "You account has been created successfuly."
msgstr ""

#: canaille/account.py:393
msgid "User account creation succeed."
msgstr ""

#: canaille/account.py:420
msgid ""
"A password initialization link has been sent at the user email address. "
"It should be received within a few minutes."
msgstr ""

#: canaille/account.py:437
msgid ""
"A password reset link has been sent at the user email address. It should "
"be received within a few minutes."
msgstr ""

#: canaille/account.py:443
msgid "Could not send the password reset email"
msgstr ""

#: canaille/account.py:477
msgid "Profile edition failed."
msgstr ""

#: canaille/account.py:513
msgid "Profile updated successfuly."
msgstr ""

#: canaille/account.py:536
#, python-format
msgid "The user %(user)s has been sucessfuly deleted"
msgstr ""

#: canaille/account.py:566
msgid "Could not send the password reset link."
msgstr ""

#: canaille/account.py:570
msgid ""
"A password reset link has been sent at your email address. You should "
"receive it within a few minutes."
msgstr ""

#: canaille/account.py:581
#, python-format
msgid ""
"The user '%(user)s' does not have permissions to update their password. "
"We cannot send a password reset email."
msgstr ""

#: canaille/account.py:596
msgid "We encountered an issue while we sent the password recovery email."
msgstr ""

#: canaille/account.py:615
msgid "The password reset link that brought you here was invalid."
msgstr ""

#: canaille/account.py:624
msgid "Your password has been updated successfuly"
msgstr ""

#: canaille/admin.py:23 canaille/templates/userlist.html:14
msgid "Email"
msgstr ""

#: canaille/admin.py:29 canaille/forms.py:49 canaille/forms.py:73
#: canaille/forms.py:145 canaille/forms.py:267
msgid "jane@doe.com"
msgstr ""

#: canaille/admin.py:42
msgid "The test invitation mail has been sent correctly"
msgstr ""

#: canaille/admin.py:44
msgid "The test invitation mail has not been sent correctly"
msgstr ""

#: canaille/admin.py:89 canaille/mails.py:180
msgid "Password initialization on {website_name}"
msgstr ""

#: canaille/admin.py:131 canaille/mails.py:140
msgid "Password reset on {website_name}"
msgstr ""

#: canaille/admin.py:173
msgid "Invitation on {website_name}"
msgstr ""

#: canaille/apputils.py:31
msgid "John Doe"
msgstr ""

#: canaille/apputils.py:34 canaille/forms.py:108 canaille/forms.py:255
msgid "jdoe"
msgstr ""

#: canaille/apputils.py:37
msgid "john@doe.com"
msgstr ""

#: canaille/apputils.py:39
msgid " or "
msgstr ""

#: canaille/flaskutils.py:63
msgid "No SMTP server has been configured"
msgstr ""

#: canaille/forms.py:17
msgid "The login '{login}' already exists"
msgstr ""

#: canaille/forms.py:24
msgid "The email '{email}' already exists"
msgstr ""

#: canaille/forms.py:31
msgid "The group '{group}' already exists"
msgstr ""

#: canaille/forms.py:40
msgid "The login '{login}' does not exist"
msgstr ""

#: canaille/forms.py:46 canaille/forms.py:70 canaille/templates/userlist.html:8
msgid "Login"
msgstr ""

#: canaille/forms.py:59 canaille/forms.py:82 canaille/forms.py:166
msgid "Password"
msgstr ""

#: canaille/forms.py:85 canaille/forms.py:170
msgid "Password confirmation"
msgstr ""

#: canaille/forms.py:88 canaille/forms.py:173
msgid "Password and confirmation do not match."
msgstr ""

#: canaille/forms.py:99
msgid "Automatic"
msgstr ""

#: canaille/forms.py:107 canaille/forms.py:254
msgid "Username"
msgstr ""

#: canaille/forms.py:111 canaille/forms.py:226 canaille/forms.py:240
#: canaille/oidc/forms.py:17 canaille/templates/groups.html:17
#: canaille/templates/oidc/admin/client_list.html:23
#: canaille/templates/userlist.html:11
msgid "Name"
msgstr ""

#: canaille/forms.py:113
msgid "Given name"
msgstr ""

#: canaille/forms.py:115
msgid "John"
msgstr ""

#: canaille/forms.py:121
msgid "Family Name"
msgstr ""

#: canaille/forms.py:124
msgid "Doe"
msgstr ""

#: canaille/forms.py:130
msgid "Display Name"
msgstr ""

#: canaille/forms.py:133
msgid "Johnny"
msgstr ""

#: canaille/forms.py:139 canaille/forms.py:260
msgid "Email address"
msgstr ""

#: canaille/forms.py:141
msgid ""
"This email will be used as a recovery address to reset the password if "
"needed"
msgstr ""

#: canaille/forms.py:151
msgid "Phone number"
msgstr ""

#: canaille/forms.py:151
msgid "555-000-555"
msgstr ""

#: canaille/forms.py:154
msgid "Address"
msgstr ""

#: canaille/forms.py:156
msgid "132, Foobar Street, Gotham City 12401"
msgstr ""

#: canaille/forms.py:160
msgid "Photo"
msgstr ""

#: canaille/forms.py:164 canaille/templates/profile.html:95
msgid "Delete the photo"
msgstr ""

#: canaille/forms.py:178
msgid "Number"
msgstr ""

#: canaille/forms.py:180
msgid "1234"
msgstr ""

#: canaille/forms.py:184
msgid "Website"
msgstr ""

#: canaille/forms.py:186
msgid "https://mywebsite.tld"
msgstr ""

#: canaille/forms.py:190
msgid "Preferred language"
msgstr ""

#: canaille/forms.py:213
msgid "users, admins ..."
msgstr ""

#: canaille/forms.py:229
msgid "group"
msgstr ""

#: canaille/forms.py:233 canaille/forms.py:247
#: canaille/templates/groups.html:18
msgid "Description"
msgstr ""

#: canaille/forms.py:258
msgid "Username editable by the invitee"
msgstr ""

#: canaille/groups.py:32
msgid "Group creation failed."
msgstr ""

#: canaille/groups.py:40
#, python-format
msgid "The group %(group)s has been sucessfully created"
msgstr ""

#: canaille/groups.py:79
#, python-format
msgid "The group %(group)s has been sucessfully edited."
msgstr ""

#: canaille/groups.py:84
msgid "Group edition failed."
msgstr ""

#: canaille/groups.py:93
#, python-format
msgid "The group %(group)s has been sucessfully deleted"
msgstr ""

#: canaille/mails.py:102
msgid "Test email from {website_name}"
msgstr ""

#: canaille/mails.py:210
msgid "You have been invited to create an account on {website_name}"
msgstr ""

#: canaille/ldap_backend/backend.py:49
msgid "Could not connect to the LDAP server '{uri}'"
msgstr ""

#: canaille/ldap_backend/backend.py:65
msgid "LDAP authentication failed with user '{user}'"
msgstr ""

#: canaille/oidc/clients.py:41
msgid "The client has not been added. Please check your information."
msgstr ""

#: canaille/oidc/clients.py:77
msgid "The client has been created."
msgstr ""

#: canaille/oidc/clients.py:121
msgid "The client has not been edited. Please check your information."
msgstr ""

#: canaille/oidc/clients.py:150
msgid "The client has been edited."
msgstr ""

#: canaille/oidc/clients.py:163
msgid "The client has been deleted."
msgstr ""

#: canaille/oidc/consents.py:40
msgid "Could not delete this access"
msgstr ""

#: canaille/oidc/consents.py:44
msgid "The access has been revoked"
msgstr ""

#: canaille/oidc/endpoints.py:129
msgid "You have been successfully logged out."
msgstr ""

#: canaille/oidc/endpoints.py:327
msgid "You have been disconnected"
msgstr ""

#: canaille/oidc/endpoints.py:335
msgid "An error happened during the logout"
msgstr ""

#: canaille/oidc/endpoints.py:347
msgid "You have not been disconnected"
msgstr ""

#: canaille/oidc/forms.py:22
msgid "Contact"
msgstr ""

#: canaille/oidc/forms.py:27
msgid "URI"
msgstr ""

#: canaille/oidc/forms.py:32
msgid "Redirect URIs"
msgstr ""

#: canaille/oidc/forms.py:37
msgid "Post logout redirect URIs"
msgstr ""

#: canaille/oidc/forms.py:42
msgid "Grant types"
msgstr ""

#: canaille/oidc/forms.py:54 canaille/templates/oidc/admin/token_view.html:33
msgid "Scope"
msgstr ""

#: canaille/oidc/forms.py:60
msgid "Response types"
msgstr ""

#: canaille/oidc/forms.py:66
msgid "Token Endpoint Auth Method"
msgstr ""

#: canaille/oidc/forms.py:76
msgid "Token audiences"
msgstr ""

#: canaille/oidc/forms.py:82
msgid "Logo URI"
msgstr ""

#: canaille/oidc/forms.py:87
msgid "Terms of service URI"
msgstr ""

#: canaille/oidc/forms.py:92
msgid "Policy URI"
msgstr ""

#: canaille/oidc/forms.py:97
msgid "Software ID"
msgstr ""

#: canaille/oidc/forms.py:102
msgid "Software Version"
msgstr ""

#: canaille/oidc/forms.py:107
msgid "JWK"
msgstr ""

#: canaille/oidc/forms.py:112
msgid "JKW URI"
msgstr ""

#: canaille/oidc/forms.py:117
msgid "Pre-consent"
msgstr ""

#: canaille/oidc/utils.py:6
msgid "Personnal information about yourself, such as your name or your gender."
msgstr ""

#: canaille/oidc/utils.py:8
msgid "Your email address."
msgstr ""

#: canaille/oidc/utils.py:9
msgid "Your postal address."
msgstr ""

#: canaille/oidc/utils.py:10
msgid "Your phone number."
msgstr ""

#: canaille/oidc/utils.py:11
msgid "Groups you are belonging to"
msgstr ""

#: canaille/templates/about.html:12 canaille/themes/default/base.html:106
msgid "About canaille"
msgstr ""

#: canaille/templates/about.html:14
msgid "Free and open-source identity provider."
msgstr ""

#: canaille/templates/about.html:17
#, python-format
msgid "Version %(version)s"
msgstr ""

#: canaille/templates/about.html:18
msgid "Homepage"
msgstr ""

#: canaille/templates/about.html:19
msgid "Documentation"
msgstr ""

#: canaille/templates/about.html:20
msgid "Source code"
msgstr ""

#: canaille/templates/error.html:19
msgid "Bad request"
msgstr ""

#: canaille/templates/error.html:21
msgid "Unauthorized"
msgstr ""

#: canaille/templates/error.html:23
msgid "Page not found"
msgstr ""

#: canaille/templates/error.html:25
msgid "Technical problem"
msgstr ""

#: canaille/templates/error.html:31
msgid "The request you made is invalid"
msgstr ""

#: canaille/templates/error.html:33
msgid "You do not have the authorizations to access this page"
msgstr ""

#: canaille/templates/error.html:35
msgid "The page you are looking for does not exist"
msgstr ""

#: canaille/templates/error.html:37
msgid "Please contact your administrator"
msgstr ""

#: canaille/templates/firstlogin.html:11
msgid "First login"
msgstr ""

#: canaille/templates/firstlogin.html:16
msgid ""
"It seems this is the first time you are logging here. In order to "
"finalize your account configuration, you need to set a password to your "
"account. We will send you an email containing a link that will allow you "
"to set a password. Please click on the \"Send the initialization email\" "
"button below to send the email."
msgstr ""

#: canaille/templates/firstlogin.html:35
#: canaille/templates/forgotten-password.html:35
msgid "Login page"
msgstr ""

#: canaille/templates/firstlogin.html:36
msgid "Send the initialization email"
msgstr ""

#: canaille/templates/fomanticui.html:68
#: canaille/templates/oidc/admin/client_edit.html:35
#: canaille/templates/oidc/admin/client_edit.html:44
#: canaille/templates/oidc/admin/client_edit.html:53
msgid "This field is not editable"
msgstr ""

#: canaille/templates/fomanticui.html:72
msgid "This field is required"
msgstr ""

#: canaille/templates/forgotten-password.html:11
#: canaille/templates/login.html:43 canaille/templates/password.html:40
msgid "Forgotten password"
msgstr ""

#: canaille/templates/forgotten-password.html:16
msgid ""
"After this form is sent, if the email address or the login you provided "
"exists, you will receive an email containing a link that will allow you "
"to reset your password."
msgstr ""

#: canaille/templates/forgotten-password.html:38
msgid "Send again"
msgstr ""

#: canaille/templates/forgotten-password.html:40
#: canaille/templates/mail/admin.html:23
msgid "Send"
msgstr ""

#: canaille/templates/group.html:22
msgid "Group deletion"
msgstr ""

#: canaille/templates/group.html:26
msgid ""
"Are you sure you want to delete this group? This action is unrevokable "
"and all the data about this group will be removed."
msgstr ""

#: canaille/templates/group.html:30
#: canaille/templates/oidc/admin/client_edit.html:18
#: canaille/templates/profile.html:41
msgid "Cancel"
msgstr ""

#: canaille/templates/group.html:31
#: canaille/templates/oidc/admin/client_edit.html:19
#: canaille/templates/profile.html:42
msgid "Delete"
msgstr ""

#: canaille/templates/group.html:40
msgid "Group creation"
msgstr ""

#: canaille/templates/group.html:42
msgid "Group edition"
msgstr ""

#: canaille/templates/group.html:48
msgid "Create a new group"
msgstr ""

#: canaille/templates/group.html:50
msgid "Edit information about a group"
msgstr ""

#: canaille/templates/group.html:67
msgid ""
"Because group cannot be empty, you will be added to the group. You can "
"remove you later by editing your profile when you will have added other "
"members to the group."
msgstr ""

#: canaille/templates/group.html:75
msgid "Delete group"
msgstr ""

#: canaille/templates/group.html:80
msgid "Create group"
msgstr ""

#: canaille/templates/group.html:82 canaille/templates/profile.html:257
msgid "Submit"
msgstr ""

#: canaille/templates/group.html:94
msgid "Group members"
msgstr ""

#: canaille/templates/groups.html:5
msgid "Add a group"
msgstr ""

#: canaille/templates/groups.html:19
msgid "Number of members"
msgstr ""

#: canaille/templates/invite.html:16
msgid "Invitation link"
msgstr ""

#: canaille/templates/invite.html:18
msgid "Invitation sent"
msgstr ""

#: canaille/templates/invite.html:20
msgid "Invitation not sent"
msgstr ""

#: canaille/templates/invite.html:29
msgid ""
"Here is the invitation link you can provide to the user you want to "
"invite:"
msgstr ""

#: canaille/templates/invite.html:36
#, python-format
msgid "This invitation link has been sent to %(email)s"
msgstr ""

#: canaille/templates/invite.html:37
msgid ""
"If you need to provide this link by other ways than email, you can copy "
"it there:"
msgstr ""

#: canaille/templates/invite.html:44
#, python-format
msgid ""
"This invitation link could not be sent to %(email)s due to technical "
"issues."
msgstr ""

#: canaille/templates/invite.html:45
msgid ""
"However you can copy the link there to provide it by other ways than "
"email:"
msgstr ""

#: canaille/templates/invite.html:58
msgid "Copy"
msgstr ""

#: canaille/templates/invite.html:65 canaille/templates/invite.html:120
msgid "Create a user"
msgstr ""

#: canaille/templates/invite.html:69
msgid "Invite another user"
msgstr ""

#: canaille/templates/invite.html:81 canaille/templates/profile.html:252
#: canaille/templates/users.html:20
msgid "Invite a user"
msgstr ""

#: canaille/templates/invite.html:87
msgid ""
"After this form is sent, the recipient your indicated will receive an "
"email containing an account creation link."
msgstr ""

#: canaille/templates/invite.html:123
msgid "Generate a link"
msgstr ""

#: canaille/templates/invite.html:126
msgid "Send the invitation"
msgstr ""

#: canaille/templates/login.html:19
#, python-format
msgid "Sign in at %(website)s"
msgstr ""

#: canaille/templates/login.html:21
msgid "Manage your information and your authorizations"
msgstr ""

#: canaille/templates/login.html:45
msgid "Continue"
msgstr ""

#: canaille/templates/password.html:19
#, python-format
msgid "Sign in as %(username)s"
msgstr ""

#: canaille/templates/password.html:21
msgid "Please enter your password for this account."
msgstr ""

#: canaille/templates/password.html:38
#, python-format
msgid "I am not %(username)s"
msgstr ""

#: canaille/templates/password.html:42
msgid "Sign in"
msgstr ""

#: canaille/templates/profile.html:18
msgid "This user cannot edit this field"
msgstr ""

#: canaille/templates/profile.html:20
msgid "This user cannot see this field"
msgstr ""

#: canaille/templates/profile.html:29
msgid "Account deletion"
msgstr ""

#: canaille/templates/profile.html:34
msgid ""
"Are you sure you want to delete this user? This action is unrevokable and"
" all the data about this user will be removed."
msgstr ""

#: canaille/templates/profile.html:36
msgid ""
"Are you sure you want to delete your account? This action is unrevokable "
"and all your data will be removed forever."
msgstr ""

#: canaille/templates/profile.html:51
msgid "User creation"
msgstr ""

#: canaille/templates/profile.html:53 canaille/themes/default/base.html:40
msgid "My profile"
msgstr ""

#: canaille/templates/profile.html:55
msgid "User profile edition"
msgstr ""

#: canaille/templates/profile.html:61
msgid "Create a new user account"
msgstr ""

#: canaille/templates/profile.html:63
msgid "Edit your personal information"
msgstr ""

#: canaille/templates/profile.html:65
msgid "Edit information about a user"
msgstr ""

#: canaille/templates/profile.html:80
msgid "Personal information"
msgstr ""

#: canaille/templates/profile.html:92 canaille/templates/profile.html:103
msgid "Click to upload a photo"
msgstr ""

#: canaille/templates/profile.html:154
msgid "Account information"
msgstr ""

#: canaille/templates/profile.html:179
msgid "User password is not mandatory"
msgstr ""

#: canaille/templates/profile.html:182
msgid "The user password can be set:"
msgstr ""

#: canaille/templates/profile.html:184
msgid "by filling this form;"
msgstr ""

#: canaille/templates/profile.html:185
msgid ""
"by sending the user a password initialization mail, after the account "
"creation;"
msgstr ""

#: canaille/templates/profile.html:186 canaille/templates/profile.html:209
msgid ""
"or simply waiting for the user to sign-in a first time, and then receive "
"a password initialization mail."
msgstr ""

#: canaille/templates/profile.html:189 canaille/templates/profile.html:212
msgid "The user will not be able to authenticate unless the password is set"
msgstr ""

#: canaille/templates/profile.html:198
msgid "Send email"
msgstr ""

#: canaille/templates/profile.html:202
msgid "This user does not have a password yet"
msgstr ""

#: canaille/templates/profile.html:205
msgid "You can solve this by:"
msgstr ""

#: canaille/templates/profile.html:207
msgid "setting a password using this form;"
msgstr ""

#: canaille/templates/profile.html:208
msgid "sending the user a password initialization mail, by clicking this button;"
msgstr ""

#: canaille/templates/profile.html:220
msgid "Send mail"
msgstr ""

#: canaille/templates/mail/admin.html:68 canaille/templates/profile.html:223
#: canaille/templates/reset-password.html:11
#: canaille/templates/reset-password.html:16
msgid "Password reset"
msgstr ""

#: canaille/templates/profile.html:225
msgid ""
"If the user has forgotten his password, you can send him a password reset"
" email by clicking this button."
msgstr ""

#: canaille/templates/profile.html:237
msgid "Delete the user"
msgstr ""

#: canaille/templates/profile.html:239
msgid "Delete my account"
msgstr ""

#: canaille/templates/profile.html:246
msgid "Impersonate"
msgstr ""

#: canaille/templates/users.html:18
msgid "Add a user"
msgstr ""

#: canaille/templates/mail/admin.html:8
msgid "Mail sending test"
msgstr ""

#: canaille/templates/mail/admin.html:13
msgid ""
"This form will send a fake invitation email to the address you want. This"
" should be used for testing mail configuration."
msgstr ""

#: canaille/templates/mail/admin.html:32
msgid "Email preview"
msgstr ""

#: canaille/templates/mail/admin.html:44 canaille/templates/mail/test.html:19
#: canaille/templates/mail/test.txt:1
msgid "Connectivity test"
msgstr ""

#: canaille/templates/mail/admin.html:56
#: canaille/templates/mail/firstlogin.html:19
#: canaille/templates/mail/reset.txt:1
msgid "Password initialization"
msgstr ""

#: canaille/templates/mail/admin.html:80
msgid "Invitation"
msgstr ""

#: canaille/templates/mail/firstlogin.html:27
#, python-format
msgid ""
"In order to finalize your account configuration at %(site_name)s, we need"
" to setup your password. Please click on the \"Initialize password\" "
"button below and follow the instructions."
msgstr ""

#: canaille/templates/mail/firstlogin.html:38
#: canaille/templates/mail/invitation.txt:5 canaille/templates/mail/reset.txt:5
msgid "Initialize password"
msgstr ""

#: canaille/templates/mail/firstlogin.txt:1
#: canaille/templates/mail/reset.html:19
msgid "Password reinitialisation"
msgstr ""

#: canaille/templates/mail/firstlogin.txt:3
#, python-format
msgid ""
"Someone, probably you, asked for a password reinitialization link at "
"%(site_name)s. If you did not asked for this email, please ignore it. If "
"you need to reset your password, please click on the link below and "
"follow the instructions."
msgstr ""

#: canaille/templates/mail/firstlogin.txt:5
#: canaille/templates/mail/reset.html:38
msgid "Reset password"
msgstr ""

#: canaille/templates/mail/invitation.html:19
#: canaille/templates/mail/invitation.txt:1
msgid "Account creation"
msgstr ""

#: canaille/templates/mail/invitation.html:27
#, python-format
msgid ""
"You have been invited to create an account at %(site_name)s. To proceed "
"you can click on the \"Create my account\" button below and follow the "
"instructions."
msgstr ""

#: canaille/templates/mail/invitation.html:38
msgid "Create my account"
msgstr ""

#: canaille/templates/mail/invitation.txt:3
#, python-format
msgid ""
"You have been invited to create an account at %(site_name)s. To proceed "
"you can click on the link below and follow the instructions."
msgstr ""

#: canaille/templates/mail/reset.html:27
#, python-format
msgid ""
"Someone, probably you, asked for a password reinitialization link at "
"%(site_name)s. If you did not ask for this email, please ignore it. If "
"you need to reset your password, please click on the \"Reset password\" "
"button below and follow the instructions."
msgstr ""

#: canaille/templates/mail/reset.txt:3
#, python-format
msgid ""
"In order to finalize your account configuration at %(site_name)s, we need"
" to setup your password. Please click on the link below and follow the "
"instructions."
msgstr ""

#: canaille/templates/mail/test.html:27 canaille/templates/mail/test.txt:3
#, python-format
msgid ""
"This email is a test to check that you receive communications from "
"%(site_name)s."
msgstr ""

#: canaille/templates/oidc/admin/authorization_list.html:18
msgid "Code"
msgstr ""

#: canaille/templates/oidc/admin/authorization_list.html:19
#: canaille/templates/oidc/admin/token_list.html:19
#: canaille/templates/oidc/admin/token_view.html:17
msgid "Client"
msgstr ""

#: canaille/templates/oidc/admin/authorization_list.html:20
#: canaille/templates/oidc/admin/token_list.html:20
#: canaille/templates/oidc/admin/token_view.html:25
msgid "Subject"
msgstr ""

#: canaille/templates/oidc/admin/authorization_list.html:21
#: canaille/templates/oidc/admin/client_list.html:25
#: canaille/templates/oidc/admin/token_list.html:21
msgid "Created"
msgstr ""

#: canaille/templates/oidc/admin/authorization_view.html:7
msgid "View an authorization"
msgstr ""

#: canaille/templates/oidc/admin/client_add.html:7
msgid "Add a client"
msgstr ""

#: canaille/templates/oidc/admin/client_add.html:11
msgid "Confirm"
msgstr ""

#: canaille/templates/oidc/admin/client_edit.html:12
msgid "Client deletion"
msgstr ""

#: canaille/templates/oidc/admin/client_edit.html:15
msgid ""
"Are you sure you want to delete this client? This action is unrevokable "
"and all the data about this client will be removed."
msgstr ""

#: canaille/templates/oidc/admin/client_edit.html:25
msgid "Edit a client"
msgstr ""

#: canaille/templates/oidc/admin/client_edit.html:32
msgid "ID"
msgstr ""

#: canaille/templates/oidc/admin/client_edit.html:41
msgid "Secret"
msgstr ""

#: canaille/templates/oidc/admin/client_edit.html:50
msgid "Issued at"
msgstr ""

#: canaille/templates/oidc/admin/client_edit.html:74
msgid "Delete the client"
msgstr ""

#: canaille/templates/oidc/admin/client_edit.html:77
msgid "Edit"
msgstr ""

#: canaille/templates/oidc/admin/client_list.html:17
msgid "Add client"
msgstr ""

#: canaille/templates/oidc/admin/client_list.html:24
msgid "URL"
msgstr ""

#: canaille/templates/oidc/admin/token_list.html:18
msgid "Token"
msgstr ""

#: canaille/templates/oidc/admin/token_view.html:7
msgid "View a token"
msgstr ""

#: canaille/templates/oidc/admin/token_view.html:13
msgid "Issue date"
msgstr ""

#: canaille/templates/oidc/admin/token_view.html:43
msgid "Audience"
msgstr ""

#: canaille/templates/oidc/admin/token_view.html:57
msgid "Revokation date"
msgstr ""

#: canaille/templates/oidc/admin/token_view.html:61
msgid "Lifetime"
msgstr ""

#: canaille/templates/oidc/admin/token_view.html:65
msgid "Token type"
msgstr ""

#: canaille/templates/oidc/admin/token_view.html:69
msgid "Access token"
msgstr ""

#: canaille/templates/oidc/admin/token_view.html:74
msgid "Refresh token"
msgstr ""

#: canaille/templates/oidc/user/authorize.html:9
#, python-format
msgid "The application %(name)s is requesting access to:"
msgstr ""

#: canaille/templates/oidc/user/authorize.html:32
#, python-format
msgid "You are logged id as: %(name)s"
msgstr ""

#: canaille/templates/oidc/user/authorize.html:39
msgid "Deny"
msgstr ""

#: canaille/templates/oidc/user/authorize.html:42
msgid "Switch user"
msgstr ""

#: canaille/templates/oidc/user/authorize.html:45
msgid "Accept"
msgstr ""

#: canaille/templates/oidc/user/consent_list.html:18
#: canaille/themes/default/base.html:47
msgid "My consents"
msgstr ""

#: canaille/templates/oidc/user/consent_list.html:21
msgid "Consult and revoke the authorization you gave to websites."
msgstr ""

#: canaille/templates/oidc/user/consent_list.html:39
msgid "From:"
msgstr ""

#: canaille/templates/oidc/user/consent_list.html:41
msgid "Revoked:"
msgstr ""

#: canaille/templates/oidc/user/consent_list.html:44
msgid "Has access to:"
msgstr ""

#: canaille/templates/oidc/user/consent_list.html:63
msgid "Remove access"
msgstr ""

#: canaille/templates/oidc/user/consent_list.html:72
msgid "You did not authorize applications yet."
msgstr ""

#: canaille/templates/oidc/user/logout.html:9
#: canaille/themes/default/base.html:90
msgid "Log out"
msgstr ""

#: canaille/templates/oidc/user/logout.html:10
msgid "Do you want to log out?"
msgstr ""

#: canaille/templates/oidc/user/logout.html:14
#, python-format
msgid "You are currently logged in as %(username)s."
msgstr ""

#: canaille/templates/oidc/user/logout.html:16
#, python-format
msgid "The application %(client_name)s wants to disconnect your account."
msgstr ""

#: canaille/templates/oidc/user/logout.html:33
msgid "Stay logged"
msgstr ""

#: canaille/templates/oidc/user/logout.html:36
msgid "Logout"
msgstr ""

#: canaille/themes/default/base.html:10
msgid "authorization interface"
msgstr ""

#: canaille/themes/default/base.html:54
msgid "Users"
msgstr ""

#: canaille/themes/default/base.html:71
msgid "Clients"
msgstr ""

#: canaille/themes/default/base.html:75
msgid "Tokens"
msgstr ""

#: canaille/themes/default/base.html:79
msgid "Codes"
msgstr ""

#: canaille/themes/default/base.html:83
msgid "Emails"
msgstr ""
